window.__PRE_CONFIG__ = {
  header: 'ServerStatus',
  subHeader: '服务器状态监控',
  interval: 1.5,
  footer: '<p>Copyright &copy; 2021 ServerStatus<br>Powered by <a href="https://github.com/cokemine/ServerStatus-Hotaru">ServerStatus-Hotaru</a></p>'
};
